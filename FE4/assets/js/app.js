
var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE','JULY'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [7, 10, 9, 12, 13, 6, 12],
      backgroundColor: 'rgba(154, 0, 201, .8)',
    },
    {
      label: 'Returned Books',
      data: [4, 7, 10, 11, 8, 8, 12],
      backgroundColor: 'rgba(247, 255, 0, .8)',
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'doughnut',
  data: {
    datasets: [{
        data: [10, 20, 30, 20],
        backgroundColor: [
          'rgb(229, 31, 31, .8)',
          'rgba(247, 255, 0, .8)',
          'rgba(159, 145, 145, .8)',
          'rgba(68, 63, 63, .8)',
        ],
    }],

    labels: [
        'Novel',
        'Fiction',
        'Biography',
        'Horror',
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});


