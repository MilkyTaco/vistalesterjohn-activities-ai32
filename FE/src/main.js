import Vue from 'vue' 
import router from './components/router'
import App from './App.vue'

//import Books from './components/pages/Books'
//import Settings from './components/pages/Settings'
//import router from './router'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'






Vue.config.productionTip = false

new Vue({
 // router,
 router,
  render: h => h(App),
}).$mount('#app')