import Vue from 'vue' 
import VueRouter from "vue-router"
import Dashboard from '@/components/HelloWorld'
import Patron from '@/components/patron'
import Books from '@/components/books'
import Settings from '@/components/settings'


Vue.use(VueRouter)

export default  new VueRouter
({
   routes: [
    {
       path: '/',
       component: Dashboard
    },
     {
        path: '/patron',
        component: Patron
     },
    {
     path: '/books',
     component: Books
    },
    {
        path: '/settings',
        component: Settings
       },
   ]
});