<?php

use App\Http\Controllers\BooksCont;
use App\Http\Controllers\Borrowed;
use App\Http\Controllers\Categories;
use App\Http\Controllers\PatronCont;
use App\Http\Controllers\ReturnedBooksCont;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('patrons', PatronCont::class)->only(['index', 'store', 'show', 'destroy', 'update']);
Route::resource('books', BooksCont::class)->only(['index', 'show', 'store', 'update', 'destroy']);

Route::resource('/borrowedbook', Borrowed::class);

Route::get('/categories', [ReturnedBooksCont::class, 'index']);
Route::get('/returned', [ReturnedBooksCont::class, 'index']);
Route::get('/returned', [ReturnedBooksCont::class, 'store']);
Route::get('/returned/{id}', [ReturnedBooksCont::class, 'show']);