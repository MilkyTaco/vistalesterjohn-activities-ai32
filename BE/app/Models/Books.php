<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'author', 'copies', 'category_id'];
    
    public function category() {
        return $this->hasOne(Category::class, 'id', 'category_id');
    } 
    
    public function borrowed() {
        return $this->hasMany(BorrowedBooks::class, 'borrowed_books', 'book_id', 'id');
    } 

    public function returned() {
        return $this->hasMany(BorrowedBooks::class, 'returned_books', 'book_id', 'id');
    } 
}
