<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    use HasFactory;

    protected $fillable = ['first_name', 'middle_name', 'last_name', 'email'];

    public function borrowed(){
        return $this->hasMany(BorrowedBooks::class, 'borrowed_books', 'patron_id', 'id');
    }

    public function returned(){
        return $this->hasMany(ReturnedBooks::class, 'patron_id', 'id');
    }
}
