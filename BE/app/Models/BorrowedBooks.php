<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BorrowedBooks extends Model
{
    use HasFactory;

    public $table = "borrowed_books";

    public function borrowedBook_book_relation() {
        return $this->belongsTo('App\Books', 'book_id', 'id');
    }

    public function borrowedBook_patron_relation() {
        return $this->belongsTo('App\Patron', 'patron_id', 'id');
    }
}
