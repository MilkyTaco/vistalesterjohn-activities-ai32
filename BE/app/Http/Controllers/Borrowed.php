<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBooks;
use App\Models\Patron;
use Illuminate\Http\Request;

class Borrowed extends Controller
{
    //
    public function index()
    {
       return response()->json(BorrowedBooks::with(['patron', 'book', 'book.category'])->get());
    }

    public function store(Request $request)
    {
       $borrowed_books = BorrowedBooks::create($request->only(['book_id', 'copies', 'patron_id']));
       $borrowedbook = BorrowedBooks::with(['books'])->find($borrowed_books->id);
       $copy = $borrowedbook->books->copy - $request->copy;
       $borrowedbook->books->update(['copies' => $copy]);
       return response()->json(['message' => 'Books borrowed sucessful', 'borrowedbooks' => $borrowedbook]);
    }

    public function show($id)
    {
       return response()->json(BorrowedBooks::with(['patron', 'book', 'books.category'])->where('id', $id)->firstorfail());
    }
}
