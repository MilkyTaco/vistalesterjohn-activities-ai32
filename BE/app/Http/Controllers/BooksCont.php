<?php

namespace App\Http\Controllers;

use App\Models\Books;
use Illuminate\Http\Request;

class BooksCont extends Controller
{
    //
    public function index()
    {
       return response()->json(Books::with(['category:id, category'])->get());
    }

    public function show($id)
    {
       $books = Books::with(['category:id, category'])->where('id', $id)->firstorfail();
       return response()->json($books);
    }

    public function store(Request $request)
    {
       return response()->json(Books::create($request->all()));
    }

    public function update(Request $request, $id)
    {
       $books = Books::with(['category:id, category'])->where('id', $id)->firstorfail();
       return response()->json($books);
       $books->update($request->all());
       return response()->json(['books' => $books]);
    }

    public function destroy($id)
    {
       return response()->json(['message', 'Books deleted successful', Books::where('id', $id)->firstorfail()->delete()]);
    }

}

