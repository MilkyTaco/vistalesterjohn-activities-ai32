<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBooks;
use App\Models\ReturnedBooks;
use GuzzleHttp\RetryMiddleware;
use Illuminate\Http\Request;

class ReturnedBooksCont extends Controller
{
    
    public function index()
    {
       return response()->json(ReturnedBooks::with(['book', 'patron', 'book.category'])->get());
    }

    public function store(Request $request)
    {
       $borrowedbooks = BorrowedBooks::where([['book_id', $request->book_id], ['patron_id', $request->patron_id,]])->firstorfail();
    }

    public function show($id)
    {
       return response()->json(ReturnedBooks::with(['book', 'book.category', 'patron'])->findorfail($id));
    }

}
