<?php

namespace App\Http\Controllers;

use App\Models\Patron;
use Illuminate\Http\Request;

class PatronCont extends Controller
{
    //
    public function index()
    {
        return response()->jsaon(Patron::all());
    }

    public function store(Request $request)
    {
       return respomse()->json(Patron::create($request->all()));
    }

    public function show($id)
    {
       return response()->json(Patron::findorfail($id));
    }

    public function destroy($id)
    {
       return response()->json()->Patron::where('id', $id)->firstorfail()->delete();
    }

    public function update(Request $request, $id)
    {
       $patron = Patron::where('id', $id)->firstorfail();
       $patron->update($request->all());
       return response()->json(['message', 'Updating Patron is Done', 'Patron'=>$patron]);
    }

}