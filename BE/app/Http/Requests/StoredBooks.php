<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\BorrowedBooks;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\ResponseException;
use Illuminate\Http\Requests;


class StoredBooks extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return 
        [
            //
            'name' => 'required|min:1|max:50',
            'author' => 'required|min:1|max:50',
            'copies' => 'required|numeric',
            'category_id' => 'required|exists:categories, id',

        ];
    }

    /**
     * 
     * 
     * @return array
     */
    public function message()
    {
       return
       [
            'name.required' => 'Name strongly required',
            'name.min' => 'Must have atleast a minimum of one character',
            'name.max' => 'Must not exceed a maximum of 50 characters',
            'author.required' => 'Author is strongly required',
            'author.min' => 'Must have atleast a minimum of one character',
            'author.max' => 'Must not exceed a maximum of 50 characters',
            'copies.required' => 'Copies are strongly required',
            'copies.numeric' => 'Copies must be in numeric format',
            'category_id.required' => 'Books must belong to a category',
            'category_id.exists' => 'Category does not exists'
       ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
