<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\BorrowedBooks;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\ResponseException;
use Illuminate\Http\Requests;

class StoredReturnedBooks extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       $borrowed = BorrowedBooks::where('book_id', request()->get('book_id'))->where('patron_id', request()->get('patron_id'));
       if(!empty($borrowed))
       {
           $copies = $borrowed->copies;
       }
       else
       {
           $copies = request()->get('copies');
       }
       return
       [
           'copies.required' => 'Copies are required',
           'copies.numeric' => 'Copies must be in numeric format',
           'patron_id.exists' => 'Patron does not exists',
       ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
