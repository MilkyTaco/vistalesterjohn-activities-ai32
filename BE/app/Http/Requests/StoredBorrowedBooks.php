<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validartion\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoredBorrowedBooks extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $books = Books::find(request()->get('book_id'));
        if(!empty($books))
        {
           $copies = $books->copies;
        }
        else
        {
           $copies = request()->get('copies');
        }
        return
        [
           'patron_id.exists' => 'Patron does not exists',
           'copies' => ['required', "lte: {$copies}", 'bail', 'gt:0'],
           'book_id.exists' => 'Books doest not exists',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
