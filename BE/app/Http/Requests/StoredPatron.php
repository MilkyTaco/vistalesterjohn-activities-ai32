<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validartion\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoredPatron extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            'first_name' => 'required|min:1|max:50',
            'middle_name' => 'required|min:1|max:50',
            'last_name' => 'required|min:1|max:50',
            'email' => 'required|unique:patrons|email',
        ];
    }

     /**
     * 
     * 
     * @return array
     */
    public function message()
    {
       return
       [
            'first_name.required' => 'Firstname strongly required',
            'first_name.min' => 'Must have atleast a minimum of one character',
            'first_name.max' => 'Must not exceed a maximum of 50 characters',
            'middle_name.required' => 'Middlename strongly required',
            'middle_name.min' => 'Must have atleast a minimum of one character',
            'middle_name.max' => 'Must not exceed a maximum of 50 characters',
            'last_name.required' => 'Lastname is strongly required',
            'last_name.min' => 'Must have atleast a minimum of one character',
            'last_name.max' => 'Must not exceed a maximum of 50 characters',
            'email.required' => 'Email is strongly required',
            'email.unique' => 'Email already taken',
       ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
